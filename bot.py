from telebot import TeleBot, types, util
import logging, os
from dotenv import load_dotenv


logging.basicConfig(filename='bot.log', filemode='w', 
                    format='%(asctime)s %(message)s', level=logging.INFO)
logger = logging.getLogger()

load_dotenv()
BOT_TOKEN = os.getenv('BOT_TOKEN')
bot = TeleBot(BOT_TOKEN)
"""client = pymongo.MongoClient('localhost:27017')
db = client.get_database('TgBot')"""
bot.set_my_commands([
    types.BotCommand('/info', 'Information about capabilies'),
    types.BotCommand('/demo', 'Demonstration')
])

@bot.message_handler(content_types='document')
def doc_handler(mes):
    logger.info(mes)
    file = bot.get_file(mes.document.file_id)
    print(file.file_path)
    with open('doc.txt', 'wb') as dest:
        dest.write(bot.download_file(file.file_path))

@bot.message_handler(content_types='text')
def text_handler(mes):
    logger.info(mes)
    bot.reply_to(mes, mes.text)

@bot.edited_message_handler()
def edit_handler(mes):
    bot.reply_to(mes, 'Тебе не стоило этого делать')
bot.infinity_polling()

#print(util.update_types)